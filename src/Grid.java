import java.util.LinkedList;


public class Grid {
	
	private static Piece[][] contents= new Piece[8][8];
	private static LinkedList<Piece> whitePieces = new LinkedList<Piece>();
	private static LinkedList<Piece> blackPieces = new LinkedList<Piece>();
	
	public static Piece[][] getContents() {
		return contents;
	}
	
	

	public static LinkedList<Piece> getWhitePieces() {return whitePieces;}
	public static LinkedList<Piece> getBlackPieces() {return blackPieces;}



	public static boolean  addPiece(Piece p){
		
		if(contents[p.getxPos()][p.getyPos()] != null){
			System.out.println("This spot is occupied");
			return false;
		}
		
		if(p.getTeam() == 0)
			whitePieces.add(p);
		else
			blackPieces.add(p);
		
		contents[p.getxPos()][p.getyPos()] = p;
		return true;
	}
	
	public static void movePiece(Piece p, int x, int y){
		
		contents[p.getxPos()][p.getyPos()] = null;	
		
		if(contents[x][y] != null){
			
			if(contents[x][y].getTeam() == 0)
				whitePieces.remove(contents[x][y]);
			else
				blackPieces.remove(contents[x][y]);
		}
		contents[x][y] = p;
		contents[x][y].setxPos(x);
		contents[x][y].setyPos(y);
	}
	

}
