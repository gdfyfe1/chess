import java.awt.Point;
import java.util.LinkedList;


public class King extends Piece{

	public King(int x, int y, int team) {
		super(x, y, team);
		
		if(team == 0)
			setName("White king");
		
		else
			setName("Black king");
	}
	
	@Override
	public LinkedList<Point> getMoveSpots(){
		
		LinkedList<Point> p = new LinkedList<Point>();
		
		for(int x = -1; x< 2; x++){
			
			for(int y = -1; y< 2; y++){
	
					
					try{
						
						if(Grid.getContents()[getxPos() + x][getyPos() + y] == null){
							
							p.add(new Point(getxPos() + x, getyPos() + y));
						}
						
						else{
							
							if(Grid.getContents()[getxPos() + x][getyPos() + y].getTeam() != getTeam()){
								p.add(new Point(getxPos() + x, getyPos() + y));
							}
							
						}
						
					}catch(Exception e){}
					
			}
		}
		
		return p;
	}

}
