import java.awt.Point;
import java.util.LinkedList;


public class Rook extends Piece{

	public Rook(int x, int y, int team) {
		super(x, y, team);
		
		if(team == 0)
			setName("White rook");
		
		else
			setName("Black rook");
	}
	
	@Override
	public LinkedList<Point> getMoveSpots(){
		
		LinkedList<Point> p = new LinkedList<Point>();
		

		for(int x = -1; x < 2; x++){
			
			for(int y = Math.abs(x) - 1;y < 3 - (Math.abs(x) * 2) + Math.abs(x) - 1; y++){
				
				for(int i = 1; i< 9; i++){
				
					try{
						
						//Works well woopee!
						if (Grid.getContents()[getxPos() + x * i][getyPos() + y * i] == null){
							p.add(new Point(getxPos() + x * i, getyPos() + y * i));
						} 
						
						
						else{
							
							if(Grid.getContents()[getxPos() + x * i][getyPos() + y * i].getTeam() != getTeam()){
								p.add(new Point(getxPos() + x * i, getyPos() + y * i));
							}
							
							i = 9;
							
						}
						
						
					}catch(Exception e){
						
					}
					
				}
			}
		}
		
		
		return p;
	}

}
