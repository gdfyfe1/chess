import java.awt.Point;
import java.util.LinkedList;

public class Knight extends Piece{

	public Knight(int x, int y, int team) {
		super(x, y, team);
		
		if(team == 0)
			setName("White knight");
		
		else
			setName("Black knight");
	}
	
	@Override
	public LinkedList<Point> getMoveSpots(){
		
		LinkedList<Point> p = new LinkedList<Point>();
		
		for(int x = -2; x< 3; x++){
			
			if(x == 0) continue;
			
			//Y modifier - y * (3 - Math.abs(x)) 
			for(int y = -1; y< 2; y +=2){
			
				try{
					
					if(Grid.getContents()[getxPos() + x][getyPos() + y * (3 - Math.abs(x))] == null){
						p.add(new Point(getxPos() + x, getyPos() + y * (3 - Math.abs(x))));
					}
					else{
						
						if(Grid.getContents()[getxPos() + x][getyPos() + y * (3 - Math.abs(x))].getTeam() != getTeam()){
							p.add(new Point(getxPos() + x, getyPos() + y * (3 - Math.abs(x))));
						}
						
					}
					
				}catch(Exception e){}
			}
		}
		
		return p;
	}

}
