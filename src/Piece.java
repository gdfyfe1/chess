import java.awt.Point;
import java.util.LinkedList;


public abstract class Piece {
	
	private int xPos, yPos, team;
	private String name;
	private boolean selected, check;
	
	private static Piece selectedPiece;

	public Piece(int x, int y, int team){
		this.xPos = x;
		this.yPos = y;
		this.team = team;
		selected = false;
	}
	
	public int getxPos() {return xPos;}
	public void setxPos(int xPos) {this.xPos = xPos;}

	public int getyPos() {return yPos;}
	public void setyPos(int yPos) {this.yPos = yPos;}

	public String getName() {return name;}
	public void setName(String name) {this.name = name;}

	public int getTeam() {return team;}
	public void setTeam(int team) {this.team = team;}

	public boolean isSelected() {return selected;}
	public void setSelected(boolean selected) {
	
	
		if(selectedPiece != null){
			selectedPiece.selected = false;
		}
		
		this.selected = selected;
		selectedPiece = this;
	}

	public static Piece getSelectedPiece() {return selectedPiece;}
	public static void setSelectedPiece(Piece selectedPiece) {Piece.selectedPiece = selectedPiece;}

	//Should not be called 
	public abstract LinkedList<Point> getMoveSpots();
	
	public static boolean isPositionSecure(int team, int x, int y){
		
		if(team == 0){
	
			
			for(int i =0; i< Grid.getBlackPieces().size(); i++){
				
				for(int n =0; n < Grid.getBlackPieces().get(i).getMoveSpots().size(); n++){
					
					if(Grid.getBlackPieces().get(i).getMoveSpots().get(n).x == x && Grid.getBlackPieces().get(i).getMoveSpots().get(n).y == y){
						
						return false;
						
					}
				}	
			}
		}
		
		else{
			
			for(int i =0; i< Grid.getWhitePieces().size(); i++){
				
				for(int n =0; n < Grid.getWhitePieces().get(i).getMoveSpots().size(); n++){
					
					if(Grid.getWhitePieces().get(i).getMoveSpots().get(n).x == x && Grid.getWhitePieces().get(i).getMoveSpots().get(n).y == y){
						
						return false;
					}
				}	
			}
			
		}
		
		return true;
		
	}
	
	public static Point getKing(int team){
		
		for(int x = 0; x < 8; x++)
			for(int y = 0; y < 8; y++){
				
				if(Grid.getContents()[x][y] instanceof King){
					if( Grid.getContents()[x][y].getTeam() == team)
					return new Point(x, y);
				}
			}
		
		System.out.println("You shouldn't be here");
		return null;
	}
	
}
