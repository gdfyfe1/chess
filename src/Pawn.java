import java.awt.Point;
import java.util.LinkedList;

public class Pawn extends Piece{

	public Pawn(int x, int y, int team){
		
		super(x, y, team);
		
		if(team == 0)
			setName("White pawn");
		
		else
			setName("Black pawn");
	}
	
	@Override
	public LinkedList<Point> getMoveSpots(){
		
		LinkedList<Point> p = new LinkedList<Point>();
		int iterations = 0, teamD;
		
		if(getTeam() == 0){
			teamD = -1;
			
			if(getyPos() == 6)
				iterations = 2;
			else
				iterations = 1;
		}
			
		else{
			teamD = 1;
			
			if(getyPos() == 1)
				iterations = 2;
			else
				iterations = 1;
		}
		
			for(int i = -1; i< 2; i+=2){
				
				try{
					
					if(Grid.getContents()[getxPos() + i][getyPos() + teamD] != null)
						if(Grid.getContents()[getxPos() + i][getyPos() + teamD].getTeam() != getTeam()){

							p.add(new Point(getxPos() + i, getyPos() + teamD));
						}
							
					
				}
				catch(Exception e){}
				
				
			}
		
			
			for(int i = 1; i < iterations + 1; i++){
				
				if(Grid.getContents()[getxPos()][getyPos() + (i * teamD)]  == null){
					p.add(new Point(getxPos(), getyPos() + (i * teamD)));
				}
			}
		return p;
	}
}
