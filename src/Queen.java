import java.awt.Point;
import java.util.LinkedList;


public class Queen extends Piece{

	public Queen(int x, int y, int team) {
		super(x, y, team);
		
		if(team == 0)
			setName("White queen");
		
		else
			setName("Black queen");
	}
	
	@Override
	public LinkedList<Point> getMoveSpots(){
		
		LinkedList<Point> p = new LinkedList<Point>();
		
		for(int x = -1; x< 2; x++){
			
			for(int y = -1; y< 2; y++){
				
				for(int i =1; i< 9; i++){
					
					try{
						
						if(Grid.getContents()[getxPos() + x * i][getyPos() + y * i] == null){
							
							p.add(new Point(getxPos() + x * i, getyPos() + y * i));
						}
						
						else{
							
							if(Grid.getContents()[getxPos() + x * i][getyPos() + y * i].getTeam() != getTeam()){
								p.add(new Point(getxPos() + x * i, getyPos() + y * i));
							}
							
							i = 9;
							
						}
						
					}catch(Exception e){}
					
				}
			}
		}
		
		return p;
	}


}
