import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JFrame;

public class apples extends JFrame implements MouseListener{

	
	private static final long serialVersionUID = 1L;	
	
	public static final String title = "Chess";			//Game title
	public static final int width = 900;				//Game width
	public static final int height = 900; 				//Game height
	
	public static final int gridX = 8;					//Number of columns 
	public static final int gridY = 8;					//Number of rows
	
	public static int currentPlayer;					//Current player. White = 0 Black = 1
	
	//Main method
	public static void main(String[] args) {
		
		new apples();
	}
	
	//apples Constructor
	public apples(){
		
		//Sets the JFrame the way I like it
		super(title);
		setSize(width, height);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		currentPlayer = 0;
		
		setupBoard();
		
		addMouseListener(this);
	}
	
	//Sets the grid to a standard chess arrangement
	public void setupBoard(){
		
		//Sets up the pawns
		for(int t = 0; t< 2; t++){
			
			for(int i =0; i<8; i++){
				
				Grid.addPiece(new Pawn(i, 6 - (5 * t), t));
			}
		}
		
		//Sets up the rooks
		for(int x = 0; x<2; x++){
			
			for(int y =0; y< 2; y++){
				
				Grid.addPiece(new Rook(x*7, y*7, 1 - y));
			}
		}
		
		//Sets up knight
		for(int x = 0; x<2; x++){
			
			for(int y =0; y< 2; y++){
				
				Grid.addPiece(new Knight(x*5 + 1, y*7, 1 - y));
			}
		}
		
		//Sets up bishops
			for(int x = 0; x<2; x++){
				
				for(int y =0; y< 2; y++){
					
					Grid.addPiece(new Bishop(x*3 + 2, y* 7 ,1 - y));
				}
			}
			
		//Sets queens and kings
		Grid.addPiece(new Queen(3, 7, 0));
		Grid.addPiece(new Queen(4, 0, 1));	
		
		Grid.addPiece(new King(4, 7, 0));
		Grid.addPiece(new King(3, 0, 1));	
	}
	
	//Main graphics method
	public void paint(Graphics g){
		
		//Clears the screen
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		
		//Draws the grid and the pieces
		for(int x =0; x< gridX; x++)
			for(int y =0; y< gridY; y++){
				
				g.setColor(Color.black);
				g.drawRect((x * 100) + 50, (y * 100) + 50, 100, 100);
				
				if(Grid.getContents()[x][y] != null){
				
					g.drawString(Grid.getContents()[x][y].getName(), (x * 100) + 70, (y * 100) + 100);
					
				}
			}
		
		//Draws that fancy orange box hingy when a piece is selected
		if(Piece.getSelectedPiece() != null){
			
			g.setColor(new Color(255, 100, 0, 33));
			
			LinkedList<Point> ll = Piece.getSelectedPiece().getMoveSpots();
			
			for(int i = 0; i< ll.size(); i++)
				g.fillRect((ll.get(i).x * 100) + 50, (ll.get(i).y * 100) + 50, 100, 100);	
			
			}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override
	public void mousePressed(MouseEvent e) {
		
		
		int x, y;
		
		x = (e.getX() - 50) / 100;		y = (e.getY() - 50) / 100;
		
		if(x < 0 || x > 7 || y< 0 || y > 7)
			return;
		
		select_movePiece(x, y);
		
		repaint();
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {}

	//Given a tile it decides whether to select or move a piece or do nothing
	public void select_movePiece(int x, int y){
		
		
		//Move the piece
		if(Piece.getSelectedPiece() != null){											//If a piece has already been selected
			
			if(Piece.getSelectedPiece().getMoveSpots().contains(new Point(x, y))){		//If the selected tile is in the selected pieces move list 
				
				
				
				Grid.movePiece(Piece.getSelectedPiece(), x, y);
				Piece.setSelectedPiece(null);
				
				System.out.println(Piece.isPositionSecure(currentPlayer, x, y));
				
				//Changes the current player and leaves the method
				if(currentPlayer == 0)
					currentPlayer = 1;
				else
					currentPlayer = 0;	
				
				return;
			}
		}
		
		
		if(Grid.getContents()[x][y] == null)											//If the tile is null there is no piece to select so return
			return;
		
		
		if(Grid.getContents()[x][y].getTeam() == currentPlayer){						//If the piece in the tile is in the correct team select it
			Grid.getContents()[x][y].setSelected(true);
		}
		
		
		
	} 

	
}
