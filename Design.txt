Java Project: Chess


Analysis


I want to design a basic chess game.
It will follow the same rules of chess which are breifly as follows:

	Two teams (black and white) play on an 8x8 board. Both player have an equal ammount 
	chess pieces (6) which each have different ways of moving around the board and can take other 
	pieces. The pieces and their movements are as follows:

		Pawn: Can usually move 1 square forward but can ove two from its starting
		line. It can only take pieces diagonaly. Players start with 8 each. If a pawn
		reaches the back enemy line, it is promoted to either a queen or a knight.

		Rook: Can move and take pieces from any distance by moving horizontaly.
		Players start with 2 each.

		Knight: Can move and take pieces by moving in a L-shape. It is the only piece that can 
		jump over other pieces. Players start with 2.

		Bishop: Can move and take pieces from any distance by moving diagonaly
		Players start with 2 each.

		Queen: Can move and take pieces by moving either horizontal or diagonaly
		Is considored the best piece. Players start with 1.

		King: Can move and take pieces in any adjancent square. If the king is trapped 
		in a checkmate the game is over. Players obvioudly start with only 1.

Special Cases:
	
	Castleing. Hard to explain ut you know what it is.
	
	The king cannot move into a position that cause it to be in an enemy piece's range.
	Also, if the moving of a piece will cause the king to be exposed, the move cannot 
	be done.

	If the enemy moves into a position where he can see the king must be protected 
	by the player, either by moving the king or by blocking the attack. This is called a
	check.

Design

I will be using JFrames to draw to the screen from the main class named apples.

Other classes are :
	Grid

	Piece (and it's subclasses:)
		Pawn
		Rook
		Knight
		Bishop
		Queen
		King

Brief summary of classes:
	
	Grid.class
		
		Will have a static 2d array of pieces on the board. This will be used to give a global indication of the state 
		of the board, allowing the main class to draw the pieces, check for collisions ect.
		Any movements on the board will go through the Grid class allowing for easy change to the array. The class 
		will provide support for said movements with the methods : addPiece(); movePiece(); removePiece(); (parameters omitted)
		There will also be a list that keeps track of all of the pieces on the board.
		
	Piece.class
		
		Piece is super class for all of the pieces in game. It includes getters and setters for gridCo-ordinates, team of piece
		





















 



